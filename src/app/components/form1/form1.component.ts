import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { IDataNode } from 'src/app/interfaces/tree.interfaces';
import { NodeService } from 'src/app/services/node.service';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class MyFormComponent implements OnInit {
  form!: FormGroup;
  alert1 = ""
  alert2 = ""

  nestedDataSource = new MatTreeNestedDataSource<IDataNode>()
  nestedTreeControl = new NestedTreeControl<IDataNode>(node => node.children)

  constructor(fb: FormBuilder,private _nodeService: NodeService) {
    this.form = fb.group({
      niveles: [, [Validators.min(1), Validators.max(100), Validators.required]],
      repeticiones: [, [Validators.min(1), Validators.max(100), Validators.required]],
    valor1: ['', [Validators.required]],
    valor2: ['', [Validators.required]],
    valor3: ['', [Validators.required]],
    valor4: ['', [Validators.required]]
    })
  }

  testNivel() {}

  ngOnInit(): void {
    
  }

  get validarNiveles(){
    if (this.form.get("niveles")?.value == null) {
      this.alert1 = "Debe ingresar un valor"
    } else if(this.form.get("niveles")?.value < 1){
      this.alert1 = "Valor Minimo 1"
    } else {
      this.alert1 = "Valor Maximo 100"
    }
    return this.form.get('niveles')?.invalid && this.form.get('niveles')?.touched;
  }

  get validarRepeticion(){
    if (this.form.get("repeticiones")?.value == null) {
      this.alert2 = "Debe ingresar un valor"
    } else if(this.form.get("repeticiones")?.value < 1){
      this.alert2 = "Valor Minimo 1"
    } else {
      this.alert2 = "Valor Maximo 100"
    }
    return this.form.get('repeticiones')?.invalid && this.form.get('repeticiones')?.touched;
  }

  agregar() {
   
    this.form.valid? this.generarDatos(): ""
  }

  hasNestedChild(i: number, node: IDataNode) {
    if(node?.children?.length == undefined ){
      return false
    } else if(node?.children?.length > 0 ){
      return true
    }else {
      return false
    }
  }

  generarDatos() {
    let {niveles, repeticiones, valor1, valor2, valor3, valor4} = this.form.value
    var valores =[valor1, valor2, valor3, valor4]
    var result:IDataNode[] = []

    

    while (niveles > 0) {
      let valorI = this.getValores(niveles,valores)
        result = this.anidacion(result, repeticiones, valorI)
      
      niveles--
    }
   
    this.nestedDataSource.data = result;
  }

  getValores(num: number, valores: string[]): string {
    if (num > 4) {
      return this.getValores(num-4, valores)
    } else {
      return valores[num-1]
    }
  }

  anidacion(res:IDataNode[], rep:number, valorI:string): IDataNode[] {
    var listHelp: IDataNode[] = []

    for (let i = 0; i < rep; i++) {
      let pattern: IDataNode={
        name: valorI,
        children: res
      }
      listHelp.push(pattern)
    }
    return listHelp
  }

}
